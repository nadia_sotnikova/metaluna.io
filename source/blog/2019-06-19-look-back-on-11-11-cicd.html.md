---
title:  "Looking back on the 11.x releases for GitLab CI/CD"
layout: layout
date:   2019-06-19
category: work
description: With GitLab 12.0 coming soon, it's a great time to reflect on all the features we've launched since 11.0.
canonical_url: https://about.gitlab.com/blog/2019/06/19/look-back-on-11-11-cicd/
---