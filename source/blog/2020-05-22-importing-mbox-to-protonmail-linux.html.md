---
title: "Importing your mbox file into ProtonMail on Linux"
date: 2020/05/22
category: general
description: Unfortunately there is no Linux tool, but you can do it yourself pretty easily.
layout: layout_blog
---

ProtonMail is great, but unfortunately as of the time of writing this they do not have a native Linux importer. If you're using gmail or any provider that you can get an mbox file from, you can get around it with a couple free, open source tools, but in
order to use the ProtonMail Bridge you'll at least need to have a paid account with them.

## Add ProtonMail to Thunderbird

First, lets add your ProtonMail account to Thunderbird:

* Install and configure [ProtonMail Bridge](https://protonmail.com/bridge/install)
* Install [Mozilla Thunderbird](https://www.thunderbird.net/) and add your ProtonMail account, being sure to add the bridge's username and password rather than your actual account username and password.
* Add your account to Thunderbird following the instructions in the [Bridge documentation](https://protonmail.com/bridge/thunderbird). Note there are a couple unexpected messages that pop up, but this is normal.
* At this point, you should see your ProtonMail messages in Thunderbird.

## Add your mbox as a Local Folder

Get your mbox file from Google Takeout (if using gmail), or from whatever provider you use. Now, let's mount the mbox as a Local Folder:

* Install the [ImportExportTools Add-On](https://addons.thunderbird.net/en-us/thunderbird/addon/importexporttools/) to Thunderbird.
* Right click on Local Folders in Thunderbird, choose ImportExportTools NG, then Import mbox file.
* After the import is done, you'll have a new local folder. One thing you might notice is that your gmail chats are there. They contain a header `X-Gmail-Labels: Chat` you can use to filter them out, but in my case I wanted to bring them over.
* To bring them into ProtonMail, simply select all the messages (ctrl-A) and either drag them all to the ProtonMail inbox, or right click, move, and select the ProtonMail inbox. Depending on how many messages you have, this can take a long time. For me, I had to restart Thunderbird before it would do this operation, but once I did there was a progress bar at the bottom showing which email it was copying. Depending on how many emails you have you may want to break this into multiple operations.

## All done

At this point all your emails are on ProtonMail, and you no longer need Thunderbird or the ProtonMail Bridge on your computer unless you want them.