---
title:  "Setting up GitLab CI for Android projects"
layout: layout
date:   2018-10-24
category: work
description: Learn how to set up GitLab CI to ensure your Android app compiles and passes tests. 
canonical_url: https://about.gitlab.com/blog/2018/10/24/setting-up-gitlab-ci-for-android-projects/
---