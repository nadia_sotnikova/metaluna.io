---
title: "There's a light over in the IndieWeb space"
date: 2020/05/25
category: general
description: A journey into the strange, beautiful, and sometimes quite terrible IndieWeb.
layout: layout_blog
---

I would like, if you may, to take you on a strange journey. Recently I've been playing around with my personal domain and website at [metaluna.io](https://metaluna.io), and in doing so came across a group of people who are trying to turn the web into something a little more privately owned, personal, and weird.

Before I discovered what was out there, I had just had the simple idea to host a small index of projects on [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) using a static site generator. I used [Middleman](https://middlemanapp.com/) since that's what I was most familiar with. GitLab Pages is wonderful, but at the same time it's primarily a free web host intended for creating developer websites; you can make it as social as you want, but it doesn't have a community built-in who is actively sharing things with each other.

I am a social creature, and after a couple months of quarantine perhaps desperately so, so I went on the hunt for something where I could meet others who were also doing little projects or creative websites. Maybe we could link our websites together? Are webrings still a thing? (turns out, no, but there is something better.)

## Enter Neocities

I'd heard of [Neocities](https://neocities.org/) once or twice before as a kind of revival of Geocities, the 90s (or so) webhost. Geocities was famous for containing the websites of people who very well may be passionate about a topic - and there were some really deep, great fan sites in particular there - but perhaps not as talented when it came to making something aesthetically pleasing. But what it lacked in mastery it made up for in sheer creativity.

Neocities sites seem to have an incredible amount of creativity when they are at their best, but can suffer a bit from nostalgia for the sake of nostalgia with some sites really only having homages to the web of yesterday. This is not really the norm, though, and the reputation Neocities has for being a retro dumping ground is a bit unfair. [Browsing their most followed sites](https://neocities.org/browse), all of these sites are actually incredibly creative even if some use nostalgia as a medium. Some of them are hiding technical complexity behind their relatively simple interfaces, and some are just unlike anything you may have ever seen in a website.

I had a ton of fun browsing through these sites and it felt like I had discovered a part of the web that I didn't even know existed. There's also a little community of people here, following and commenting to each other. Each site hosted on Neocities gets an activity page ([here](https://neocities.org/site/metaluna) is the one for this blog) which contains an activity stream, the sites you're following, and comments/activity from people who engage with your site. It's pretty inviting and friendly.

## More, more, more

All of this was amazing to find, but I couldn't quite shake the feeling that my own little personal technical blog didn't quite fit in with the surreal, abstract websites that were most popular on Neocities. It's not that the hosting wasn't working well or that I didn't appreciate the activity page. I'm still hosted there after all, but I didn't think most of the people who read my content would naturally be browsing Neocities. So, I started looking for more options to connect with people away from the usual social networks.

This was the moment when I found the [IndieWeb organization](https://indieweb.org/), and their slightly inscrutable [getting started](https://indieweb.org/Getting_Started) page. What were all these strange things that you can do to your website? Does anyone actually use them, or am I looking at some out of date list for a long-forgotten project? Who knows.. but if you give me a checklist (and they have a [really great checklist](https://indieweb.org/IndieMark)) I can follow it to completion before realizing I never asked myself if any of this is actually worthwhile.

I took my poor little Middleman site and implemented (awfully) [web sign on](https://indieweb.org/Web_sign-in). I added [permalinks](https://indieweb.org/permalinks). I made [h-entries for everything](https://indieweb.org/h-entry), although now that I'm thinking about it, I might have forgotten this one. My website is free range for [search robots](https://indieweb.org/search). I even implemented an [h-card](https://indieweb.org/h-card) on the homepage. It's all written procedurally and sort of wedged in there, but it worked. I had RSS feeds of everything. Was it adding any value anywhere? I still have no idea really, but it was actually pretty fun to get working. [IndieWebify.me](https://indiewebify.me/) made a lot of the testing pretty easy.

## POSSE up

At this point I had a pretty nice homepage. I'd styled it up, met a few nice people on Neocities, completed a bunch of IndieWeb feature twiddling in case anyone ever showed up who noticed it, and was feeling pretty good about myself. But surely there was more, and this is where I discovered [POSSE](https://indieweb.org/POSSE), which is actually where I had my a-ha moment and realized how great this stuff is.

POSSE (Publish on your Own Site, Syndicate Elsewhere) is pretty core to the idea of the IndieWeb as I understand it. What it means is that you write content on your own blog, and then replicate that to wherever your audience may be. For some that means sending it to Medium, to Twitter, or wherever else. But the important bit is that the canonical source of your content is your own website in your own files. Nobody can go down, ban you, or accidentally delete it except maybe for you in that last case.

For me, the blog syndication part was not that useful: I already wasn't publishing anywhere special and didn't have an audience, so there was nothing really to set up. Notes, on the other hand, were a really interesting way to set things up so I could post short messages to LinkedIn and Twitter originating from my own page. And, in what seemed like magic, I was able to get likes and replies _back on my own website_.

## It's all connected

The secret to wiring together the IndieWeb is [webmentions](https://github.com/converspace/webmention/blob/master/README.md), which are like little interconnected pings between independent islands. In order to have your site listen for webmentions, though, you need a server always running to receive these messages. With a static website, I didn't have anything like that, but fortunately there were a trio of (totally free) services that were able to help:

- [webmention.io](https://webmention.io/) provides a free listener that will record any webmentions it receives, which you can access any time. I used this to implement a check whenever I build my static site. You could do this at runtime.
- [brid.gy](https://brid.gy/) is a service that will listen to Twitter, Instagram, Flickr, GitHub, Mastodon, Meetup, and Reddit and send any webmentions back to your endpoint for recording.
- [micro.blog](https://micro.blog/) allows you to set up monitoring of the RSS feeds on your site and repost them to Twitter, LinkedIn, Medium, Mastodon, and/or Tumbler. It also offers its own tiny blogging platform with webmentions and other IndieWeb features built in, but I'm not using that since I'm building out my own little site.

## What's next

Well, I'm on level one point something of [IndieMark](https://indieweb.org/IndieMark) so there's a lot to play with there. When I was reading ahead to the later stages, it looks like you more and more get involved with the community to determine how the specifications are going to involve, which is really cool. I've also joined [#IndieWeb on Freenode](https://indieweb.org/IRC) to try to meet others also interested in the topic. This will actually be my first syndicated blog post, so we'll see how that goes.

The next technical project for me to take on will be to implement the ability to [send webmentions](https://indieweb.org/Webmention) from my site - right now I'm only receiving them. If you're interested you can see my progress getting all of this working in the [public GitLab repo](https://gitlab.com/jyavorska/metaluna.io) for this site. I also want to check out [are.na](https://www.are.na) which seems like an interesting content discovery tool something like the Neocities site browser.

Or, if you'd like me to write a technical article explaining any of the things I've already implemented, let me know.

## One of us

In the end, I'm really glad the IndieWeb is out there as a kind of light in the darkness of what can otherwise seem like a more or less completely corporate daily web experience. It's weird in a good way. It's not corporate at all. It's rough around the edges and not tuned for maximum engagement. There are interesting people.. I've already connected with a few who are doing all kinds of creative things.

I feel like I've found a cozy little corner where people are following their passion, connecting with each other, and building creative things together. Long live the IndieWeb!