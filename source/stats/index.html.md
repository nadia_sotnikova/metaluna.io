---
title: "Stats"
description: "Page view stats for this site, updated daily."
---
<b>CI Status</b><br>
<br>
<a href="https://gitlab.com/personal-jyavorska/metaluna.io/-/pipelines"><img src="https://gitlab.com/personal-jyavorska/metaluna.io/badges/master/pipeline.svg"></a>
<br><br>
<b>Article views</b>
<br>
<canvas id="myChart" width="400" height="300"></canvas>
<script type="text/javascript" src="/javascript/chart.min.js"></script>
<script type="text/javascript" src="/javascript/chartjs-plugin-colorschemes.min.js"></script>
<script type="text/javascript" src="/javascript/stats.js"></script>
